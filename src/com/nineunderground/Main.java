package com.nineunderground;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import com.nineunderground.Utils.Options;
import com.nineunderground.views.MyView;

public class Main extends Activity {
	
	public static final int DIALOG_GAME_HELP = 0;
	
	private static AlertDialog alert;
	
	Options optionsGame	=	new Options();
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Quitamos la barra de titulo
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        MyView miVista	=	new MyView(getBaseContext());
        miVista.invalidate();
        //miVista.invalidate();
        
        //setContentView(R.layout.main);
        setContentView(miVista);
        
        Bitmap b = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888);
        Canvas can = new Canvas(b);
        
        Paint p = new Paint();
		p.setColor(Color.RED);
		
		can.drawLine(1, 1, 101, 101, p);
        
        miVista.draw(can);
        miVista.invalidate();
        
        setContentView(miVista);
        
        //setButtonsBehaviour();
        
        //getConfigOptions();
        
    }
    
    private void getConfigOptions(){
    	
    	SharedPreferences preferences;
    	preferences = getBaseContext().getSharedPreferences("MrRat", 0);
    	
    	int levelMode = preferences.getInt("DIFFICULT", 1);
    	
    	optionsGame.setLevel(levelMode);
    	
    }
    
    private void setButtonsBehaviour(){
    	
    	Button btnGame	=	(Button)findViewById(R.id.game_button);
    	
    	btnGame.setOnClickListener( new OnClickListener() {
			
			public void onClick(View v) {
				
				// Open Game Activity
				Intent myIntent = new Intent();
				myIntent.setClassName("com.nineunderground", "com.nineunderground.AJuego");
				myIntent.putExtra("DIFFICULT", optionsGame.getLevel() );
				
				/*
				Intent settingsActivity = new Intent(getBaseContext(),AOptions.class);
				startActivity(settingsActivity);
				*/
				
				// startActivity(myIntent);
				
			}
			
		});
    	
    	Button btnHelp	=	(Button)findViewById(R.id.help_button);
    	
    	btnHelp.setOnClickListener( new OnClickListener() {
			
			public void onClick(View v) {
				
				// Open Help dialog
				showDialog(Main.DIALOG_GAME_HELP);
				
			}
			
		});
    	
    	Button btnOptions	=	(Button)findViewById(R.id.options_button);
    	
    	btnOptions.setOnClickListener( new OnClickListener() {
			
			public void onClick(View v) {
				
				// Open Options Activity
				Intent myIntent = new Intent();
				myIntent.setClassName("com.nineunderground", "com.nineunderground.AOptions");
				
				startActivity(myIntent);
				
			}
			
		});
    	
    }
    
    protected AlertDialog onCreateDialog(int id) {
    	
    	alert = null;
    	
    	switch(id) {
    	
    		case DIALOG_GAME_HELP:
    			
    			AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
				
				builder2.setTitle("HELP");
				builder2.setIcon(R.drawable.player_01 );
				
				builder2.setMessage(R.string.help_message);
				
				builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
			        	
						//System.out.println("en PositiveButton");
						
			        }
					
				});
				
				alert = builder2.create();
    		
    		break;
    	
    	}
    	
    	return alert;
    	
    }
    
    public void onActivityResult(int requestCode, int resultCode, Intent data){
    	
		if (requestCode == 1 ) {

            if (resultCode == RESULT_OK) {
               
            }
		}
    	
    }
    
}