package com.nineunderground.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.nineunderground.AJuego;
import com.nineunderground.R;

public class BlockView extends View {

	private Drawable mDrawable;
	
	private int x;
	private int y;
	private int width;
	private int height;
	
	public BlockView(Context context, int xPos, int yPos) {
		
		super(context);
		
		x = xPos;
        y = yPos;
        width = 88;
        height = 80;
		
        mDrawable = context.getResources().getDrawable(R.drawable.icon_block);
        mDrawable.setBounds(x, y, x + width, y + height);
        
	}
	
	protected void onDraw(Canvas canvas) {
        mDrawable.draw(canvas);
    }
	
	public boolean setLocationByDirection(int direction)
    {
		boolean movePossible = false;
        
		switch(direction){
		
			case(AJuego.RIGHT):
				
				//if(this.x + this.width > 790){
				//	break;
				//}
				
				this.x = this.x + this.width;
		        mDrawable.setBounds(x, y, x + width, y + height);
		        this.invalidate();
				
		        movePossible = true;
				break;
			
			case(AJuego.LEFT):
			
				//if(this.x - this.width < 0){
				//	break;
				//}
			
				this.x = this.x - this.width;
		        mDrawable.setBounds(x, y, x + width, y + height);
		        this.invalidate();
				
		        movePossible = true;
				break;
			
			case(AJuego.DOWN):
				
				//if(this.y + this.height > 320){
				//	break;
				//}
				
				this.y = this.y + this.height;
		        mDrawable.setBounds(x, y, x + width, y + height);
		        this.invalidate();
				
		        movePossible = true;
				break;			
			
			case(AJuego.UP):
				
				//if(this.y - this.height < 0){
				//	break;
				//}
				
				this.y = this.y - this.height;
				mDrawable.setBounds(x, y, x + width, y + height);
		        this.invalidate();
				
		        movePossible = true;
				break;			
			
			default:
		
		}
		
		return movePossible;
		
    }

}
