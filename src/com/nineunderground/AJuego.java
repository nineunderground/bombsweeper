package com.nineunderground;

import java.util.StringTokenizer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.nineunderground.views.BlockView;
import com.nineunderground.views.CheeseView;
import com.nineunderground.views.PlayerView;

public class AJuego extends Activity{
	
	// Object arrays
	public 	String		[]playerCellsList;
	public 	String		[]blocksCellsList;
	public 	String		[]cheeseCellsList;
	
	public 	int[][]		arrayPanel		=	new int[9][5];
	public int	timeGame	=	0;
	
	// Cells values
	public static final int NO_BLOCK		= 0;
	public static final int MR_RAT_FIGURE	= 1;
	public static final int ROCK_FIGURE		= 2;
	public static final int CHEESE_FIGURE	= 3;
	
	// Movement constants
	public static final int RIGHT = 1;
	public static final int LEFT = 2;
	public static final int DOWN = 3;
	public static final int UP = 4;
	
	// Dialogs
	public static final int DIALOG_LEVEL_CLEAR	= 1;
	public static final int DIALOG_GAME_CLEAR	= 2;
	public static final int DIALOG_GAME_OVER	= 3;
	
	// Menu options
	private static final int MENU_RESET_LEVEL = 1;
	private static final int MENU_END_GAME = 2;
	
	public	static	final	int	MESSAGE_CHANGE_SECONDS = 0;
	public	static	final	int	MESSAGE_END_GAME	=	1;
	
	public Handler mHandler;
	
	private static AlertDialog alert;
	
	public int level			=	0;
	public int difficultValue	=	1;
	public int initialTime		=	0;
	
	 
	public int widthScreen;
	public int heightScreen;
	
	private float fPlayerLastPosX = 0;
	private float fPlayerLastPosY = 0;
	
	private float fPlayerActualPosX = 0;
	private float fPlayerActualPosY = 0;
	
	private int[] intMrRatColRow = {0,0};
	
	private int nextSquare = 0;
	private int nextMoveSquare = 0;
	
	private String prefixBlock = "9";
	
	public Thread	timeThread;
	
	public boolean stopThread = false;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Quitamos la barra de titulo
        requestWindowFeature(Window.FEATURE_NO_TITLE);        
        
        //setContentView(R.layout.game);
        setContentView(R.layout.game_frame);
        
        // Get Display config
        Display display = getWindowManager().getDefaultDisplay(); 
    	this.widthScreen = display.getWidth();
    	this.heightScreen = display.getHeight();
        
    	// Get options config
    	setOptionsConfig();
    	
        // Setting level objects
    	settingLevel(level);
        
    	// Setting initial score & time
    	settingEmptyScoreTime();
    	
    	// throws time decreasing
        throwTimeThread();
        
        // manage handler events
        manageHandler();
        
    }
    
    private void setOptionsConfig(){
    	
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    	String livesPref = prefs.getString("TimePref", "18");
    	
    	initialTime = Integer.parseInt(livesPref);
    	
//    	Intent valuesFromMain = getIntent();
//    	difficultValue = valuesFromMain.getIntExtra("DIFFICULT",1);
//    	
//    	switch(difficultValue){
//    	
//	    	case 1:
//	    		
//	    		// Time for level is 50 seconds
//	    		initialTime = 50;
//	    		
//	    		break;
//	    		
//	    	case 2:
//	    		
//	    		// Time for level is 35 seconds
//	    		initialTime = 35;
//	    		
//	    		break;
//	    		
//	    	case 3:
//    		
//	    		// Time for level is 20 seconds
//	    		initialTime = 20;
//	    		
//	    		break;
//	    	
//	    	case 4:
//	    		
//	    		// Time for level is 15 seconds
//	    		initialTime = 15;
//	    		
//	    		break;
//	    		
//    		default:
//    			
//    			break;
//    	
//    	}
    	
    }
    
    private void manageHandler(){
    	
    	mHandler = new Handler() {
    		
    		@Override
            public void handleMessage(Message msg) {
    			
    			switch (msg.what){
    			
    				case MESSAGE_END_GAME:
    					
    					System.out.println("Se recoge el evento de GAME OVER");
    					timeThread.stop();
    					showDialog(AJuego.DIALOG_GAME_OVER);
    					break;
    					
    				case MESSAGE_CHANGE_SECONDS:
    					
    					// Se actualiza el campo time con el valor de timeGame
    					TextView	text = (TextView)findViewById(R.id.time);
    			    	String time  = String.valueOf(timeGame);
    			    	text.setText("TIME-".concat(time));
    			    	
    			    	//timeGame = Integer.parseInt(time);
    			
    			}
    			
    		}
    		
    	};
    	
    }
    
    /**
     * Sets initial scores & time
     */
    private void settingEmptyScoreTime(){
    	
    	// Get maximum score from prefs
    	
    	TextView text;
    	text = (TextView)findViewById(R.id.player_score);
    	text.setText("1P-000000");
    	/*
    	 * CAMBIAR
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		prefs.getString("valueScorePref", "000000");
    	*/
    	
    	text = (TextView)findViewById(R.id.top_score);
    	text.setText("TOP-000000");
    	
    	
    	
    	// Get time by dificult from prefs
    	text = (TextView)findViewById(R.id.time);
    	String time  = String.valueOf(initialTime);
    	text.setText("TIME-".concat(time));
    	
    	timeGame = initialTime;
    	
    }
    
    /**
     * Updates score & time
     */
    private void updateScoreTime(boolean isReset){
    	
    	TextView	textPlayerScore	=	(TextView)findViewById(R.id.player_score);
    	TextView 	textTopScore	=	(TextView)findViewById(R.id.top_score);
    	TextView	textTime		=	(TextView)findViewById(R.id.time);
    	
    	if(!isReset){
    	
	    	// SCORE
	    	String actualValue = (String)textPlayerScore.getText();
	    	actualValue = (String)actualValue.subSequence(3, 9);  // FORMAT: "1P-001423"
	    	int actualPlayerScore = Integer.parseInt(actualValue);
	    	
	    	actualValue = (String)textTime.getText();
	    	actualValue = (String)actualValue.subSequence(5, 7); // FORMAT: "TIME-99"
	    	actualPlayerScore = actualPlayerScore + Integer.parseInt(actualValue);
	    	
	    	String newScore = String.valueOf(actualPlayerScore);
	    	// Fill newScore with zeros on the left
	    	for(int cont = newScore.length() + 1 ; cont<7; cont++){
	    		newScore = "0".concat(newScore);
	    	}
	    	textPlayerScore.setText( "1P-".concat(newScore) );
	    	
	    	// TOP
	    	actualValue = (String)textTopScore.getText();
	    	actualValue = (String)actualValue.subSequence(4, 10); // FORMAT: "TOP-999999"
	    	int topScore = Integer.parseInt(actualValue);
	    	
	    	// Sets top score if it has been reached
	    	if( actualPlayerScore > topScore ){
	    		textTopScore.setText("TOP-".concat(newScore) );
	    		
	    		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
	    		SharedPreferences.Editor editor = prefs.edit();
				editor.putString("valueScorePref", "000000");
				editor.commit();
	    		
	    	}
    	
    	}
    	
    	// TIME
    	String time	= String.valueOf(initialTime); // Get from prefs
    	textTime.setText("TIME-".concat(time));
    	
    	timeGame = initialTime;
    	
    }
    
    public void settingMrRat(int l){
    	
    	playerCellsList = getResources().getStringArray(R.array.MrRatLevelsList);
    	StringTokenizer tokenCasilla	=	new StringTokenizer( playerCellsList[l],";");
        
        // Format "0:0;"
        StringTokenizer tokencoordenada	=	new StringTokenizer( tokenCasilla.nextToken() ,":");
        
        int coor_colum	=	Integer.parseInt(tokencoordenada.nextToken());
        int coor_row	=	Integer.parseInt(tokencoordenada.nextToken());
        
        arrayPanel[coor_colum][coor_row]	=	AJuego.MR_RAT_FIGURE;
        
        intMrRatColRow[0] = coor_colum;
        intMrRatColRow[1] = coor_row;
        
    	FrameLayout	frmLayout	=	(FrameLayout)findViewById(R.id.main_frame);
    	
    	int	width	=	(this.widthScreen / 9 )  * coor_colum;
    	int height	=	((this.heightScreen - 80) / 5 ) * coor_row;
    	
        PlayerView vwPlayer	=	new PlayerView(this,width,height);
        
        // vwPlayer debe escuchar eventos
        vwPlayer.setId(AJuego.MR_RAT_FIGURE);
    	frmLayout.addView(vwPlayer);
    	
    }
    
    public void settingBlocks(int l){
    	
    	FrameLayout	frmLayout	=	(FrameLayout)findViewById(R.id.main_frame);
    	blocksCellsList = getResources().getStringArray(R.array.BlockLevelsList);
    	
    	// Format "0:0;1:3;3:4;.......;n"
    	StringTokenizer tokenCasillas	=	new StringTokenizer( blocksCellsList[l],";");

    	StringTokenizer tokencoordenada;
    	int coor_colum;
    	int coor_row;
    	int width;
    	int height;
    	
    	BlockView vwBlock;
    	
        while (tokenCasillas.hasMoreTokens()){
            
            tokencoordenada	=	new StringTokenizer( tokenCasillas.nextToken() ,":");
            
            coor_colum	=	Integer.parseInt(tokencoordenada.nextToken());
            coor_row	=	Integer.parseInt(tokencoordenada.nextToken());
            
            // Sets rock block
            arrayPanel[coor_colum][coor_row]	=	AJuego.ROCK_FIGURE;
            
            width	=	(this.widthScreen / 9 )  * coor_colum;
        	height	=	((this.heightScreen - 80) / 5 ) * coor_row;
        	
            vwBlock	=	new BlockView(this,width,height);
            
            String coordenadas = String.valueOf(coor_colum).concat(String.valueOf(coor_row));
            coordenadas = prefixBlock.concat(coordenadas);
            vwBlock.setId(Integer.parseInt(coordenadas));
            
            frmLayout.addView(vwBlock);
            
        }
        
    }
    
    public void settingCheese(int l){
    	
    	cheeseCellsList = getResources().getStringArray(R.array.CheeseLevelsList);
    	StringTokenizer tokenCasilla	=	new StringTokenizer( cheeseCellsList[l],";");
        
        // Format "8:4;"
        StringTokenizer tokencoordenada	=	new StringTokenizer( tokenCasilla.nextToken() ,":");
        
        int coor_colum	=	Integer.parseInt(tokencoordenada.nextToken());
        int coor_row	=	Integer.parseInt(tokencoordenada.nextToken());
        
        arrayPanel[coor_colum][coor_row]	=	AJuego.CHEESE_FIGURE;
        
    	FrameLayout	frmLayout	=	(FrameLayout)findViewById(R.id.main_frame);

    	int	width	=	(this.widthScreen / 9 )  * coor_colum;
    	int height	=	((this.heightScreen - 80) / 5 ) * coor_row;
    	
    	CheeseView vwCheese	=	new CheeseView(this,width,height);
    	vwCheese.setId(AJuego.CHEESE_FIGURE);
        frmLayout.addView(vwCheese);
        
    }
    
    private void throwTimeThread(){
    	
    	timeThread = new Thread(new Runnable() {
    		
    		public void run() {
    			
    			while(!stopThread && timeGame > 0){
    				
    				try {
        				// Every second level time decrease 1
    					Thread.currentThread().sleep(1000);
    				} catch (InterruptedException e) {
    					
    				}
    				
    				timeGame--;
    				mHandler.sendEmptyMessage(AJuego.MESSAGE_CHANGE_SECONDS);
    				
    				System.out.println("Un segundo menos...");
    				
    			}
    			
    			if(timeGame == 0){
    				System.out.println("Se envia al manejador el mensaje de GAME OVER");
        			mHandler.sendEmptyMessage(AJuego.MESSAGE_END_GAME);
    			}
    			
    		}
    		
    	});
    	
    	timeThread.start();
    	
    }

    // BACK Button
    public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			//Log.d(this.getClass().getName(), "back button pressed");
			stopThread = true;
			finish();
		}
		
		return false;
	}
    
    // MENU OPTIONS
    public boolean onCreateOptionsMenu(Menu menu){
    	
    	super.onCreateOptionsMenu(menu);
    	menu.setQwertyMode(true);
    	
    	MenuItem mnu1 = menu.add(0, MENU_RESET_LEVEL, 0, "RESET LEVEL");
        {
            mnu1.setAlphabeticShortcut('r');
            mnu1.setIcon(R.drawable.reset);            
        }
        
        MenuItem mnu2 = menu.add(0, MENU_END_GAME, 0, "QUIT GAME");
        {
        	mnu2.setAlphabeticShortcut('q');
        	mnu2.setIcon(R.drawable.go_back);            
        }
        
        return true;
    	
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
    	
    	switch (item.getItemId()) {
    	
	    	case MENU_RESET_LEVEL:
	        	
	    		settingLevel(level);
	    		updateScoreTime(true);
	    		
	    		return true;
	    		
	    	case MENU_END_GAME:
	    		
	    		stopThread = true;
	    		finish();
	    		return true;
	    		
	    	default:
    	
    	}
    	
        return false;
    	
    }
    
    //Setting level
    private void settingLevel(int l){
    	
    	cheeseCellsList = getResources().getStringArray(R.array.CheeseLevelsList);
    	
    	if( l == cheeseCellsList.length ){
    		// No more levels
    		showDialog(AJuego.DIALOG_GAME_CLEAR);
    		return;
    	}
    	
    	FrameLayout	frmLayout	=	(FrameLayout)findViewById(R.id.main_frame);
    	String coordenadas;
    	
    	// Reset game panel array
    	for(int cont=0; cont<arrayPanel.length; cont++ ){
    		for(int cont2=0; cont2<arrayPanel[cont].length; cont2++){
    			
    			if( arrayPanel[cont][cont2] == AJuego.ROCK_FIGURE ){
    				
    				coordenadas	=	String.valueOf(cont).concat(String.valueOf(cont2));
    				coordenadas = prefixBlock.concat(coordenadas);
    	            frmLayout.removeView(findViewById(Integer.parseInt(coordenadas)));
    	            
    			}
    			
    			arrayPanel[cont][cont2] = AJuego.NO_BLOCK;
    		}
    	}
    	
    	// Remove older views
    	frmLayout.removeView(findViewById(AJuego.CHEESE_FIGURE));
    	frmLayout.removeView(findViewById(AJuego.MR_RAT_FIGURE));
    	
    	settingCheese(level);
    	settingMrRat(level);
        settingBlocks(level);
    	
    }

    /**
     * Gets alert with animation showing level clear
     */
    public void levelFinished(){
    	
    	System.out.println("Juego terminado. Paramos el hilo");
    	//timeThread.stop();
    	stopThread = true;
    	
    	TextView	textTime		=	(TextView)findViewById(R.id.time);
    	String	actualTime = (String)textTime.getText();
    	actualTime = (String)actualTime.subSequence(5, 7); // FORMAT: "TIME-99"
    	
    	int timeLeft = Integer.parseInt(actualTime);
    	if( timeLeft > 0 ){
    		showDialog(AJuego.DIALOG_LEVEL_CLEAR);
    	}
    	
    }
    
    @Override
    protected void onPrepareDialog(final int id, final Dialog dialog) {
    	
    	switch (id) {
    		case AJuego.DIALOG_LEVEL_CLEAR:
	    	    
    			//update level
    			level = level + 1;
    			
    			AlertDialog d = (AlertDialog)dialog;
    			d.setMessage("You have complete level " + level);
    			
	    	    break;
    	    
    	    default:
    	    
    	    	break;
    	    
    	}
    	
    }
    
    /**
     * Creation dialogues 
     */
    protected AlertDialog onCreateDialog(int id) {
    	
    	alert = null;
    	
    	switch(id) {
        
			case AJuego.DIALOG_LEVEL_CLEAR:
				
				AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
				
				builder2.setTitle("LEVEL CLEAR");
				builder2.setIcon(R.drawable.player_01 );
				
				builder2.setMessage("You have complete level " + level);
				
				builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
			        	
						//System.out.println("en PositiveButton");
						settingLevel(level);
						updateScoreTime(false);
						stopThread = false;
						throwTimeThread();
						
			        }
					
				});
				
				alert = builder2.create();
				
				break;
				
			case AJuego.DIALOG_GAME_CLEAR:
				
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				
				builder.setTitle("GAME COMPLETE");
				builder.setIcon(R.drawable.player_01 );
				
				builder.setMessage("Congratulations! All levels complete");
				
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
			        	
						//System.out.println("Game Over");
						finish();

			        }
					
				});
				
				AJuego.alert = builder.create();
				
			case AJuego.DIALOG_GAME_OVER:
				
				System.out.println("DIALOG_GAME_OVER");
				AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
				
				builder3.setTitle("GAME OVER");
				builder3.setIcon(R.drawable.player_01 );
				
				builder3.setMessage("Oh! There is no more time. Let's try again!");
				
				builder3.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int id) {
			        	
						//System.out.println("Game Over");
						finish();

			        }
					
				});
				
				AJuego.alert = builder3.create();
				
			default:
    	
    	}
    	
    	return alert;
    	
    }
    
    //  TOUCH EVENTS
    @Override
    public boolean onTouchEvent(MotionEvent motion) {
    	
    	switch (motion.getAction()) {

			case (MotionEvent.ACTION_UP): // MotionEvent.ACTION_MOVE
				
				fPlayerActualPosX = motion.getX();
				fPlayerActualPosY = motion.getY();
				
				float vectorX	=	fPlayerActualPosX - fPlayerLastPosX; // el 1er movimiento LastPosX = 0
				float vectorY	=	fPlayerActualPosY - fPlayerLastPosY; // el 1er movimiento LastPosY = 0
				
				if( ( Math.abs(vectorX) >=  Math.abs(vectorY) ) && vectorX > 0 ){				// DERECHA
					
					//Toast.makeText(getBaseContext(), "movimiento DERECHA de: " + Float.toString(fPlayerActualPosX-fPlayerLastPosX) , 500).show();
					
					nextSquare = intMrRatColRow[0] + 1 ;
					
					// El siguiente bloque no se sale de la pantalla
					if( nextSquare > 8 ){
						//System.out.println("No se puede realizar movimiento");
					}else{
						
						// El siguiente bloque no es una roca
						if( (arrayPanel[nextSquare][intMrRatColRow[1]] == AJuego.ROCK_FIGURE) ){
							
							nextMoveSquare = nextSquare + 1;
							
							// Se puede mover el bloque al hueco siguiente?
							// Dos bloques mas no se sale del limite y ademas es un bloque vacio
							if( (nextMoveSquare < 9) && (arrayPanel[nextMoveSquare][intMrRatColRow[1]] == AJuego.NO_BLOCK) ){
								
								//System.out.println("Movemos el bloque y despues a Mr Rat");
								
								String coordenadas	=	String.valueOf(nextSquare).concat(String.valueOf(intMrRatColRow[1]));
								coordenadas = prefixBlock.concat(coordenadas);
					            BlockView vwBlock	=	(BlockView)findViewById(Integer.parseInt(coordenadas));
					            
					            // La roca se mueve a la derecha
					            if(vwBlock.setLocationByDirection(AJuego.RIGHT)){
					            	coordenadas	=	String.valueOf(nextMoveSquare).concat(String.valueOf(intMrRatColRow[1]));
					            	coordenadas = prefixBlock.concat(coordenadas);
					            	vwBlock.setId(Integer.parseInt(coordenadas));
					            }
					            
								// Mr Rat se mueve a la derecha
								PlayerView vwPlayer		=	(PlayerView)findViewById(AJuego.MR_RAT_FIGURE);
								if(vwPlayer.setLocationByDirection(AJuego.RIGHT)){
									
									intMrRatColRow[0] = intMrRatColRow[0] + 1; // Mr Rat position updated 
									
									if(arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] == AJuego.CHEESE_FIGURE){
									      
										//System.out.println("Nivel terminado");
										levelFinished();
									}
									
								}
								
								// Se actualizan los valores de arrayPanel
								arrayPanel[intMrRatColRow[0] - 1][intMrRatColRow[1]]	=	AJuego.NO_BLOCK;		// Se pone bloque vacio
								arrayPanel[intMrRatColRow[0]    ][intMrRatColRow[1]]	=	AJuego.MR_RAT_FIGURE;	// Se pone Mr Rat
								arrayPanel[intMrRatColRow[0] + 1][intMrRatColRow[1]]	=	AJuego.ROCK_FIGURE;		// Se pone la roca
								
							}
							
						}else{
							
							//Se mueve el personaje Mr Rat
							PlayerView vwPlayer		=	(PlayerView)findViewById(AJuego.MR_RAT_FIGURE);
							if(vwPlayer.setLocationByDirection(AJuego.RIGHT)){
								
								arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] = AJuego.NO_BLOCK;
								intMrRatColRow[0] = intMrRatColRow[0] + 1; // Mr Rat position updated 
								
								if(arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] == AJuego.CHEESE_FIGURE){
								      
									//System.out.println("Nivel terminado");
									levelFinished();
								}
								
							}
							
						}
						
					}
					
				}else if(( Math.abs(vectorX) >=  Math.abs(vectorY) ) && vectorX < 0 ){		// IZQUIERDA
					
					//Toast.makeText(getBaseContext(), "movimiento IZQUIERDA de: " + Float.toString(fPlayerActualPosX-fPlayerLastPosX) , 500).show();
					
					// Se puede realizar movimiento?
					nextSquare = intMrRatColRow[0] - 1 ;
					//System.out.println("Probamos a la izquierda. Hacia " + nextSquare);
					if(nextSquare < 0 ){
						//System.out.println("No se puede realizar movimiento");
					}else{
						//System.out.println("Se puede. Hacia " + nextSquare);
						// El siguiente bloque no es una roca
						if( (arrayPanel[nextSquare][intMrRatColRow[1]] == AJuego.ROCK_FIGURE) ){
							
							nextMoveSquare = nextSquare - 1;
							//System.out.println("Probamos a mover a la izquierda. Hacia " + nextMoveSquare);
							// Se puede mover el bloque al hueco siguiente?
							// Dos bloques mas no se sale del limite y ademas es un bloque vacio
							if( (nextMoveSquare > -1) && (arrayPanel[nextMoveSquare][intMrRatColRow[1]] == AJuego.NO_BLOCK) ){
								
								//System.out.println("Movemos el bloque y despues a Mr Rat");
								
								String coordenadas	=	String.valueOf(nextSquare).concat(String.valueOf(intMrRatColRow[1]));
								coordenadas = prefixBlock.concat(coordenadas);
					            BlockView vwBlock	=	(BlockView)findViewById(Integer.parseInt(coordenadas));
					            
					            // La roca se mueve a la derecha
					            if(vwBlock.setLocationByDirection(AJuego.LEFT)){
					            	coordenadas	=	String.valueOf(nextMoveSquare).concat(String.valueOf(intMrRatColRow[1]));
					            	coordenadas = prefixBlock.concat(coordenadas);
					            	vwBlock.setId(Integer.parseInt(coordenadas));
					            }
					            
								// Mr Rat se mueve a la derecha
								PlayerView vwPlayer		=	(PlayerView)findViewById(AJuego.MR_RAT_FIGURE);
								if(vwPlayer.setLocationByDirection(AJuego.LEFT)){
									
									arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] = AJuego.NO_BLOCK;
									intMrRatColRow[0] = intMrRatColRow[0] - 1; // Mr Rat position updated 
									
									if(arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] == AJuego.CHEESE_FIGURE){
									      
										//System.out.println("Nivel terminado");
										levelFinished();
									}
									
								}
								
								// Se actualizan los valores de arrayPanel
								arrayPanel[intMrRatColRow[0] + 1][intMrRatColRow[1]]	=	AJuego.NO_BLOCK;		// Se pone bloque vacio
								arrayPanel[intMrRatColRow[0]    ][intMrRatColRow[1]]	=	AJuego.MR_RAT_FIGURE;	// Se pone Mr Rat
								arrayPanel[intMrRatColRow[0] - 1][intMrRatColRow[1]]	=	AJuego.ROCK_FIGURE;		// Se pone la roca
								
							}
							
						}else{
							
							//Se mueve el personaje Mr Rat
							PlayerView vwPlayer		=	(PlayerView)findViewById(AJuego.MR_RAT_FIGURE);
							if(vwPlayer.setLocationByDirection(AJuego.LEFT)){
								
								arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] = AJuego.NO_BLOCK;
								intMrRatColRow[0] = intMrRatColRow[0] - 1;
								
								if(arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] == AJuego.CHEESE_FIGURE){
								      
									//System.out.println("Nivel terminado");
									levelFinished();
								}
								
							}
						}
					}
					
				}else if(( Math.abs(vectorX) <  Math.abs(vectorY) ) && vectorY > 0 ){			// ABAJO
					
					//Toast.makeText(getBaseContext(), "movimiento ABAJO de: " + Float.toString(fPlayerActualPosY-fPlayerLastPosY) , 500).show();
					
					nextSquare = intMrRatColRow[1] + 1 ;
					//System.out.println("Hacia abajo?? hacia " + nextSquare);
					if(nextSquare > 4 ){ //  || (arrayPanel[intMrRatColRow[0]][nextSquare] == AJuego.ROCK_FIGURE) 
						//System.out.println("No se puede realizar movimiento");
					}else{
						
						// El siguiente bloque no es una roca
						if( (arrayPanel[intMrRatColRow[0]][nextSquare] == AJuego.ROCK_FIGURE) ){
							
							nextMoveSquare = nextSquare + 1;
							//System.out.println("A mover la roca hacia " + nextMoveSquare);
							// Se puede mover el bloque al hueco siguiente?
							// Dos bloques mas no se sale del limite y ademas es un bloque vacio
							if( (nextMoveSquare < 5) && (arrayPanel[intMrRatColRow[0]][nextMoveSquare] == AJuego.NO_BLOCK) ){
								
								String coordenadas	=	String.valueOf(intMrRatColRow[0]).concat(String.valueOf(nextSquare));
								coordenadas = prefixBlock.concat(coordenadas);
					            BlockView vwBlock	=	(BlockView)findViewById(Integer.parseInt(coordenadas));
					            
					            // La roca se mueve a la derecha
					            if(vwBlock.setLocationByDirection(AJuego.DOWN)){
					            	//System.out.println("Las coordenadas del nuevo bloque " + String.valueOf(intMrRatColRow[0]).concat(String.valueOf(nextMoveSquare)));
					            	coordenadas	=	String.valueOf(intMrRatColRow[0]).concat(String.valueOf(nextMoveSquare));
					            	coordenadas = prefixBlock.concat(coordenadas);
					            	vwBlock.setId(Integer.parseInt(coordenadas));
					            }
					            
								// Mr Rat se mueve a la derecha
								PlayerView vwPlayer		=	(PlayerView)findViewById(AJuego.MR_RAT_FIGURE);
								if(vwPlayer.setLocationByDirection(AJuego.DOWN)){
									
									arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] = AJuego.NO_BLOCK;
									intMrRatColRow[1] = intMrRatColRow[1] + 1; // Mr Rat position updated 
									
									if(arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] == AJuego.CHEESE_FIGURE){
									      
										//System.out.println("Nivel terminado");
										levelFinished();
									}
									
								}
								
								// Se actualizan los valores de arrayPanel
								arrayPanel[intMrRatColRow[0]][intMrRatColRow[1] - 1]	=	AJuego.NO_BLOCK;		// Se pone bloque vacio
								arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]    ]	=	AJuego.MR_RAT_FIGURE;	// Se pone Mr Rat
								arrayPanel[intMrRatColRow[0]][intMrRatColRow[1] + 1]	=	AJuego.ROCK_FIGURE;		// Se pone la roca
								
							}
							
						}else{
							
							PlayerView vwPlayer		=	(PlayerView)findViewById(AJuego.MR_RAT_FIGURE);
							if(vwPlayer.setLocationByDirection(AJuego.DOWN)){
								
								arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] = AJuego.NO_BLOCK;
								intMrRatColRow[1] = intMrRatColRow[1] + 1;
								
								if(arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] == AJuego.CHEESE_FIGURE){
								      
									//System.out.println("Nivel terminado");
									levelFinished();
								}
								
							}
							
						}
						
					}
					
				}else if( ( Math.abs(vectorX) <  Math.abs(vectorY) ) && vectorY < 0 ){		// ARRIBA
					
					//Toast.makeText(getBaseContext(), "movimiento ARRIBA de: " + Float.toString(fPlayerActualPosY-fPlayerLastPosY) , 500).show();
					
					nextSquare = intMrRatColRow[1] - 1 ;
					
					if(nextSquare < 0 ){
						//System.out.println("No se puede realizar movimiento");
					}else{
						
						if  (arrayPanel[intMrRatColRow[0]][nextSquare] == AJuego.ROCK_FIGURE){
							
							nextMoveSquare = nextSquare - 1;
							//System.out.println("A mover la roca hacia " + nextMoveSquare);
							// Se puede mover el bloque al hueco siguiente?
							// Dos bloques mas no se sale del limite y ademas es un bloque vacio
							if( (nextMoveSquare > -1) && (arrayPanel[intMrRatColRow[0]][nextMoveSquare] == AJuego.NO_BLOCK) ){
								
								String coordenadas	=	String.valueOf(intMrRatColRow[0]).concat(String.valueOf(nextSquare));
								coordenadas = prefixBlock.concat(coordenadas);
								BlockView vwBlock	=	(BlockView)findViewById(Integer.parseInt(coordenadas));
					            //System.out.println("ENCONTRADA");
					            // La roca se mueve a la arriba
					            if(vwBlock.setLocationByDirection(AJuego.UP)){
					            	coordenadas	=	String.valueOf(intMrRatColRow[0]).concat(String.valueOf(nextMoveSquare));
					            	coordenadas = prefixBlock.concat(coordenadas);
					            	vwBlock.setId(Integer.parseInt(coordenadas));
					            }
					            
								// Mr Rat se mueve a la arriba
								PlayerView vwPlayer		=	(PlayerView)findViewById(AJuego.MR_RAT_FIGURE);
								if(vwPlayer.setLocationByDirection(AJuego.UP)){
									
									arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] = AJuego.NO_BLOCK;
									intMrRatColRow[1] = intMrRatColRow[1] - 1; // Mr Rat position updated 
									
									if(arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] == AJuego.CHEESE_FIGURE){
									      
										//System.out.println("Nivel terminado");
										levelFinished();
									}
									
								}
								
								// Se actualizan los valores de arrayPanel
								arrayPanel[intMrRatColRow[0]][intMrRatColRow[1] + 1]	=	AJuego.NO_BLOCK;		// Se pone bloque vacio
								arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]    ]	=	AJuego.MR_RAT_FIGURE;	// Se pone Mr Rat
								arrayPanel[intMrRatColRow[0]][intMrRatColRow[1] - 1]	=	AJuego.ROCK_FIGURE;		// Se pone la roca
								
							}
							
						}else{
							
							PlayerView vwPlayer		=	(PlayerView)findViewById(AJuego.MR_RAT_FIGURE);
							if(vwPlayer.setLocationByDirection(AJuego.UP)){
								
								arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] = AJuego.NO_BLOCK;
								intMrRatColRow[1] = intMrRatColRow[1] - 1;
								
								if(arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] == AJuego.CHEESE_FIGURE){
								      
									//System.out.println("Nivel terminado");
									levelFinished();
								}
								
							}
							
						} 
						
					}
					
				}
			
			case (MotionEvent.ACTION_DOWN):
				
				// Update last position coordinates
				fPlayerLastPosX = motion.getX();
				fPlayerLastPosY = motion.getY();
				
	    	default:
				
				// Do nothing

    	}

    	return super.onTouchEvent(motion);
    	
    }
    
}    