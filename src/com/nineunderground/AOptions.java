package com.nineunderground;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.widget.Toast;


/**
 * @author inaki
 *
 */
public class AOptions extends PreferenceActivity{

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        
        // Quitamos la barra de titulo
        //requestWindowFeature(Window.FEATURE_NO_TITLE);

        addPreferencesFromResource(R.xml.prefs);
        
        setPreferencesConfig();
        
    }
    
    private void setPreferencesConfig(){
    	
    	//SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    	
    	//final String livesPref = prefs.getString("livesPref", "90");
    	
    	// RESET TOP SCORES
    	Preference customPref = (Preference) findPreference("resetScorePref");
        customPref.setOnPreferenceClickListener(
        		new OnPreferenceClickListener(){
        			public boolean onPreferenceClick(Preference preference) {
        				
        				// Se lanzaria un dialog para confirmar
        				Toast.makeText(getBaseContext(),"Top score reset",Toast.LENGTH_SHORT).show();
        				
        				//Toast.makeText(getBaseContext(),"Selected " + livesPref + " lifes",Toast.LENGTH_SHORT).show();
        				
        				/*
        				SharedPreferences customSharedPreference = getSharedPreferences("myCustomSharedPrefs", Activity.MODE_PRIVATE);
        				SharedPreferences.Editor editor = customSharedPreference.edit();
        				editor.putString("myCustomPref","The preference has been clicked");
        				editor.commit();
        				*/
        				return true;
        				
        			}
        		}
        );
        
    }
    
}