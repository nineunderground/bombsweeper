package com.nineunderground.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.nineunderground.R;

public class CheeseView extends View {

	private Drawable mDrawable;
	
	public CheeseView(Context context, int xPos, int yPos) {
		
		super(context);
		
		int x = xPos;
        int y = yPos;
        int width = 88;
        int height = 80;
		
        mDrawable = context.getResources().getDrawable(R.drawable.icon_cheese);
        mDrawable.setBounds(x, y, x + width, y + height);
        
	}
	
	protected void onDraw(Canvas canvas) {
        mDrawable.draw(canvas);
    }

}
