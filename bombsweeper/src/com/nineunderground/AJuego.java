package com.nineunderground;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.nineunderground.views.BlockView;
import com.nineunderground.views.CheeseView;
import com.nineunderground.views.PlayerView;

public class AJuego extends Activity{
	
	public 	String		[]playerCellsList;
	public 	String		[]blocksCellsList;
	public 	String		[]cheeseCellsList;
	
	public 	int[][]		arrayPanel		=	new int[9][5];
	
	public static final int NO_BLOCK		= 0;
	public static final int MR_RAT_FIGURE	= 1;
	public static final int ROCK_FIGURE		= 2;
	public static final int CHEESE_FIGURE	= 3;
	
	// Movement constants
	public static final int RIGHT = 1;
	public static final int LEFT = 2;
	public static final int DOWN = 3;
	public static final int UP = 4;
	
	public	List<ImageView> listaImagenes = new ArrayList<ImageView>(45);
	
	public int level	=	0;
	 
	public int widthScreen;
	public int heightScreen;
	
	private float fPlayerLastPosX = 0;
	private float fPlayerLastPosY = 0;
	
	private float fPlayerActualPosX = 0;
	private float fPlayerActualPosY = 0;
	
	private int[] intMrRatColRow = {0,0};
	
	private int nextSquare = 0;
	private int nextMoveSquare = 0;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Quitamos la barra de titulo
        requestWindowFeature(Window.FEATURE_NO_TITLE);        
        
        //setContentView(R.layout.game);
        setContentView(R.layout.game_frame);
        
        // Get Display config
        Display display = getWindowManager().getDefaultDisplay(); 
    	this.widthScreen = display.getWidth();
    	this.heightScreen = display.getHeight();
        
        // Setting Mr Rat, Blocks & Cheese
    	settingCheese();
    	settingMrRat();
        settingBlocks();
        
        //throwTimeThread();
        
    }
    
    public void settingMrRat(){
    	
    	playerCellsList = getResources().getStringArray(R.array.MrRatLevelsList);
    	StringTokenizer tokenCasilla	=	new StringTokenizer( playerCellsList[level],";");
        
        // Format "0:0;"
        StringTokenizer tokencoordenada	=	new StringTokenizer( tokenCasilla.nextToken() ,":");
        
        int coor_colum	=	Integer.parseInt(tokencoordenada.nextToken());
        int coor_row	=	Integer.parseInt(tokencoordenada.nextToken());
        
        arrayPanel[coor_colum][coor_row]	=	AJuego.MR_RAT_FIGURE;
        
        intMrRatColRow[0] = coor_colum;
        intMrRatColRow[1] = coor_row;
        
    	FrameLayout	frmLayout	=	(FrameLayout)findViewById(R.id.main_frame);
    	
    	int	width	=	(this.widthScreen / 9 )  * coor_colum;
    	int height	=	((this.heightScreen - 80) / 5 ) * coor_row;
    	
        PlayerView vwPlayer	=	new PlayerView(this,width,height);
        
        // vwPlayer debe escuchar eventos
        vwPlayer.setId(AJuego.MR_RAT_FIGURE);
    	frmLayout.addView(vwPlayer);
    	
    }
    
    public void settingBlocks(){
    	
    	FrameLayout	frmLayout	=	(FrameLayout)findViewById(R.id.main_frame);
    	blocksCellsList = getResources().getStringArray(R.array.BlockLevelsList);
    	
    	// Format "0:0;1:3;3:4;.......;n"
    	StringTokenizer tokenCasillas	=	new StringTokenizer( blocksCellsList[level],";");

    	StringTokenizer tokencoordenada;
    	int coor_colum;
    	int coor_row;
    	int width;
    	int height;
    	
    	BlockView vwBlock;
    	
        while (tokenCasillas.hasMoreTokens()){
            
            tokencoordenada	=	new StringTokenizer( tokenCasillas.nextToken() ,":");
            
            coor_colum	=	Integer.parseInt(tokencoordenada.nextToken());
            coor_row	=	Integer.parseInt(tokencoordenada.nextToken());
            
            // Sets rock block
            arrayPanel[coor_colum][coor_row]	=	AJuego.ROCK_FIGURE;
            
            width	=	(this.widthScreen / 9 )  * coor_colum;
        	height	=	((this.heightScreen - 80) / 5 ) * coor_row;
        	
            vwBlock	=	new BlockView(this,width,height);
            
            String coordenadas = String.valueOf(coor_colum).concat(String.valueOf(coor_row));
            vwBlock.setId(Integer.parseInt(coordenadas));
            
            frmLayout.addView(vwBlock);
            
        }
        
    }
    
    public void settingCheese(){
    	
    	cheeseCellsList = getResources().getStringArray(R.array.CheeseLevelsList);
    	StringTokenizer tokenCasilla	=	new StringTokenizer( cheeseCellsList[level],";");
        
        // Format "8:4;"
        StringTokenizer tokencoordenada	=	new StringTokenizer( tokenCasilla.nextToken() ,":");
        
        int coor_colum	=	Integer.parseInt(tokencoordenada.nextToken());
        int coor_row	=	Integer.parseInt(tokencoordenada.nextToken());
        
        arrayPanel[coor_colum][coor_row]	=	AJuego.CHEESE_FIGURE;
        
    	FrameLayout	frmLayout	=	(FrameLayout)findViewById(R.id.main_frame);

    	int	width	=	(this.widthScreen / 9 )  * coor_colum;
    	int height	=	((this.heightScreen - 80) / 5 ) * coor_row;
    	
    	CheeseView vwCheese	=	new CheeseView(this,width,height);
    	vwCheese.setId(AJuego.CHEESE_FIGURE);
        frmLayout.addView(vwCheese);
    	
    }
    
    private void throwTimeThread(){
    	
    }
    
    //  TRACKPAD EVENTS
    @Override
    public boolean onTrackballEvent(MotionEvent motion) {
    	
    	switch (motion.getAction()) {

			case (MotionEvent.ACTION_MOVE):
				
				fPlayerActualPosX = motion.getX();
				fPlayerActualPosY = motion.getY();
				
				if( fPlayerActualPosX-fPlayerLastPosX > 1.0){
					
					nextSquare = intMrRatColRow[0] + 1 ;
					
					if( nextSquare > 8 ){
						//System.out.println("No se puede realizar movimiento");
					}else{
						
						if( (arrayPanel[nextSquare][intMrRatColRow[1]] == AJuego.ROCK_FIGURE) ){
							
							// Se puede mover el bloque al hueco siguiente?
							nextMoveSquare = nextSquare + 1;
							
							if( (nextMoveSquare > 8) || (arrayPanel[nextMoveSquare][intMrRatColRow[1]] == AJuego.NO_BLOCK) ){
								
								System.out.println("Movemos el bloque y despues a Mr Rat");
								
								// Se mueve el bloque y se mueve a Mr Rat
								//arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] = AJuego.ROCK_FIGURE;
								
								// Se desplaza el bloque
								
								String coordenadas	=	String.valueOf(nextSquare).concat(String.valueOf(intMrRatColRow[1]));
					            BlockView vwBlock	=	(BlockView)findViewById(Integer.parseInt(coordenadas));
					            
					            if(vwBlock.setLocationByDirection(AJuego.RIGHT)){
					            	
					            }
					            
								// Se desplaza a Mr Rat 
								PlayerView vwPlayer		=	(PlayerView)findViewById(AJuego.MR_RAT_FIGURE);
								if(vwPlayer.setLocationByDirection(AJuego.RIGHT)){
									
									//arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] = AJuego.NO_BLOCK;
									intMrRatColRow[0] = intMrRatColRow[0] + 1; // Mr Rat position updated 
									
									if(arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] == AJuego.CHEESE_FIGURE){
									      
										System.out.println("Nivel terminado");
										
									}
									
								}
								
								// Se actualizan los valores de arrayPanel
								arrayPanel[intMrRatColRow[0] + 2][intMrRatColRow[1]]	= AJuego.ROCK_FIGURE;	// Se pone la roca
								arrayPanel[intMrRatColRow[0] + 1][intMrRatColRow[1]]	= AJuego.MR_RAT_FIGURE; // Se pone Mr Rat
								arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]]		= AJuego.NO_BLOCK;		// Se pone bloque vacio
								
							}
							
						}else{
							
							PlayerView vwPlayer		=	(PlayerView)findViewById(AJuego.MR_RAT_FIGURE);
							if(vwPlayer.setLocationByDirection(AJuego.RIGHT)){
								
								arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] = AJuego.NO_BLOCK;
								intMrRatColRow[0] = intMrRatColRow[0] + 1; // Mr Rat position updated 
								
								if(arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] == AJuego.CHEESE_FIGURE){
								      
									System.out.println("Nivel terminado");
									
								}
								
							}
							
						}
						
					}
					
				}else if(fPlayerActualPosX-fPlayerLastPosX < -1.0 ){
					
					// Se puede realizar movimiento?
					nextSquare = intMrRatColRow[0] - 1 ;
					
					if((nextSquare < 0 ) || (arrayPanel[nextSquare][intMrRatColRow[1]] == AJuego.ROCK_FIGURE) ){
						//System.out.println("No se puede realizar movimiento");
					}else{
						
						PlayerView vwPlayer		=	(PlayerView)findViewById(AJuego.MR_RAT_FIGURE);
						if(vwPlayer.setLocationByDirection(AJuego.LEFT)){
							
							intMrRatColRow[0] = intMrRatColRow[0] - 1;
							
							if(arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] == AJuego.CHEESE_FIGURE){
							      
								System.out.println("Nivel terminado");
								
							}
							
						}
						
					}
					
				}else if(fPlayerActualPosY-fPlayerLastPosY > 1.0 ){
					
					nextSquare = intMrRatColRow[1] + 1 ;
					
					if((nextSquare > 4 ) || (arrayPanel[intMrRatColRow[0]][nextSquare] == AJuego.ROCK_FIGURE) ){
						//System.out.println("No se puede realizar movimiento");
					}else{
						
						PlayerView vwPlayer		=	(PlayerView)findViewById(AJuego.MR_RAT_FIGURE);
						if(vwPlayer.setLocationByDirection(AJuego.DOWN)){
							
							intMrRatColRow[1] = intMrRatColRow[1] + 1;
							
							if(arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] == AJuego.CHEESE_FIGURE){
							      
								System.out.println("Nivel terminado");
								
							}
							
						}
						
					}
					
				}else if(fPlayerActualPosY-fPlayerLastPosY < -1.0 ){
					
					nextSquare = intMrRatColRow[1] - 1 ;
					
					if((nextSquare < 0 ) || (arrayPanel[intMrRatColRow[0]][nextSquare] == AJuego.ROCK_FIGURE) ){
						//System.out.println("No se puede realizar movimiento");
					}else{
						
						PlayerView vwPlayer		=	(PlayerView)findViewById(AJuego.MR_RAT_FIGURE);
						if(vwPlayer.setLocationByDirection(AJuego.UP)){
							
							intMrRatColRow[1] = intMrRatColRow[1] - 1;
							
							if(arrayPanel[intMrRatColRow[0]][intMrRatColRow[1]] == AJuego.CHEESE_FIGURE){
							      
								System.out.println("Nivel terminado");
								
							}
							
						}
						
					}
					
				}else{
					//System.out.println("Movimiento insuficiente");
				}
				
				//System.out.println("Mr Rat esta en columna: " + intMrRatColRow[0] + " fila: " + intMrRatColRow[1]);
				
				break;
			
	    	default:
				
				// Do nothing

    	}

    	return super.onTrackballEvent(motion);
    	
    }
    
}    