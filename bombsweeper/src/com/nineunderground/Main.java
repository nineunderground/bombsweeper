package com.nineunderground;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Main extends Activity {
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Quitamos la barra de titulo
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.main);
        
        getButtonsConfig();
    }
    
    private void getButtonsConfig(){
    	
    	Button btnGame	=	(Button)findViewById(R.id.game_button);
    	
    	btnGame.setOnClickListener( new OnClickListener() {
			
			public void onClick(View v) {
				
				// Open Game Activity
				Intent myIntent = new Intent();
				myIntent.setClassName("com.nineunderground", "com.nineunderground.AJuego");
				startActivity(myIntent);
				
			}
			
		}
        );
    	
    }
    
    public void onActivityResult(int requestCode, int resultCode, Intent data){
    	
		if (requestCode == 1 ) {

            if (resultCode == RESULT_OK) {
               
            }
		}
    	
    }
    
}